<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.04.2017
 * Time: 22:38
 */

class AUTH {
    public static function sessionStart($start = false) {
        $lifeTime = 86400;
        if (isset($_COOKIE[session_name()]) || $start) {
            if (!session_id()) session_start();
            if (isset($_SESSION['starttime'])) {
                if (time() - $_SESSION['starttime'] >= $lifeTime) {
                    session_unset();
                    session_regenerate_id(true);
                }
            } else {
                $_SESSION['starttime'] = time();
            }
        }
    }

    public static function autentificationCookies() {
        self::sessionStart();
        if (!self::isAuth()) {
            if (isset($_COOKIE['Id']) && isset($_COOKIE['userHash'])) {
                $user_id = intval($_COOKIE['user_id']);
                $userHash = $_COOKIE['userHash'];
                $checkHash = self::checkHash($user_id, $userHash);
                if ($checkHash !== false) {
                    $user_nick = $checkHash['user_email'];
                    $user_pass = $checkHash['user_pass'];
                    self::setCookieUser($user_id, $user_nick, $user_pass);
                    self::sessionStart(true);
                    $_SESSION['user_id'] = $user_id;
                    $_SESSION['user_nick'] = $user_nick;
                } else {
                    self::logout();
                }
            }
        }
    }

    private static function checkHash($user_id, $userHash) {
        $connect = DB_CONNECT::getInstance();
        $user_id = intval($user_id);
        $userHash = $connect->escape($userHash);
        $query = "SELECT user_nick, user_pass FROM user WHERE user_id = $user_id LIMIT 1";
        if ($result = $connect->query($query)) {
            if ($result->num_rows > 0) {
                if ($row = $result->fetch_assoc()) {
                    $userLoginDB = $row['user_email'];
                    $userPassDB = $row['user_pass'];
                    $userHashDB = md5($userLoginDB . $userPassDB);
                    if ($userHashDB == $userHash) {
                        return $row;
                    } else {
                        LOG::sendLog("В куках он авторизованн, на деле хэш пароля не совпал [$user_id][$userLoginDB][$userPassDB]");
                        self::clearCookieUser();
                    }
                }
            } else {
                LOG::sendLog("В куках он авторизованн, на деле такого пользователя нет [$user_id]");
                self::clearCookieUser();
            }
        } else LOG::sendLog("checkHash($user_id, $userHash)", $connect->err());
        return false;
    }

    public static function getUserByLogin($user_nick) {
        $connect = DB_CONNECT::getInstance();
        $user_nick = $connect->escape(trim($user_nick));
        $results = Array();
        if (!isLen($user_nick, 1, 256)) $return['error'] = "Введите логин от 1 до 256 символов.";
        else {
            $query =
                "SELECT user_id, user_nick, user_pass " .
                "FROM user " .
                "WHERE UPPER(user_nick) LIKE UPPER('$user_nick')";
            if (!$result = $connect->query($query)) {
                $results['error'] = "Ошибка запроса учетной записи.";
                LOG::sendLog("getUserByLogin($user_nick)", $connect->err());
            } else {
                if ($result->num_rows == 0) {
                    $results['error'] = "Не найдена учетная запись с таким логином.";
                    LOG::sendLog("getUserByLogin($user_nick), Не найден пользователь");
                } else {
                    $row = $result->fetch_assoc();
                    $results = $row;
                }
            }
        }
        return $results;
    }

    public static function autentification($user_nick, $user_pass) {
        $connect = DB_CONNECT::getInstance();
        $user_nick = $connect->escape(trim($user_nick));
        $user_pass = $connect->escape(trim($user_pass));
        $return = Array();
        $user_pass = md5($user_pass);
        $user = AUTH::getUserByLogin($user_nick);
        if (isErr($user)) $return = $user;
        else {
            $user_id = $user['user_id'];
            $userPassDB = $user['user_pass'];
            if ($userPassDB != $user_pass) {
                $return['error'] = "Ошибка авторизации. Неверный пароль.";
                LOG::sendLog("autentification($user_nick, $user_pass), неверный пароль");
            } else {
                self::setCookieUser($user_id, $user_nick, $user_pass);
                self::sessionStart(true);
                $_SESSION['user_id'] = $user_id;
                $_SESSION['user_nick'] = $user_nick;
            }
        }
        return $return;
    }

    public static function registration($user_name, $user_family, $user_nick, $user_pass, $user_pass_r) {
        $connect = DB_CONNECT::getInstance();
        $return = Array();

        $user_name = $connect->escape(trim($user_name));
        $user_family = $connect->escape(trim($user_family));
        $user_nick = $connect->escape(trim($user_nick));
        $user_pass = $connect->escape(trim($user_pass));
        $user_pass_r = $connect->escape(trim($user_pass_r));

        if ($user_pass != $user_pass_r) {
            $return['error'] = "Ваши пароли не совпадают";
        } else {
            $user_pass = md5($user_pass);
            $user = AUTH::getUserByLogin($user_nick);
            if (!isErr($user)) $return['error'] = "Данный ник занят."; else {
                $fields = "user_name, user_family, user_nick, user_pass, user_date_add";
                $data = "'$user_name', '$user_family', '$user_nick', '$user_pass', NOW()";
                /** @var TYPE_NAME $fields */
                $query = "INSERT INTO user ($fields) VALUES ($data)";
                if (!$result = $connect->query($query)) {
                    $return['error'] = "Ошибка регистрации .. ";
                } else {
                    $user_id = $connect->insertId();
                    self::setCookieUser($user_id, $user_nick, $user_pass);
                    self::sessionStart(true);
                    $_SESSION['user_id'] = $user_id;
                    $_SESSION['user_nick'] = $user_nick;
                }
            }
        }
        return $return;
    }

    public static function logout() {
        self::clearCookieUser();
        if (self::isAuth()) {
            session_unset();
            $params = session_get_cookie_params();
            setcookie(session_name(), "", time() - 3600 * 24 * 30 * 12, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
            session_destroy();
        }
    }

    private static function setCookieUser($user_id, $user_email, $user_pass) {
        setcookie("user_id", $user_id, time() + 60 * 60 * 24 * 30, "/");
        setcookie("userHash", md5($user_email . $user_pass), time() + 60 * 60 * 24 * 30, "/");
    }

    private static function clearCookieUser() {
        setcookie("user_id", "", time() - 3600 * 24 * 30 * 12, "/");
        setcookie("userHash", "", time() - 3600 * 24 * 30 * 12, "/");
    }

    public static function isAuth() {
        if (session_id()) {
            if (isset($_SESSION['user_id'])) return true;
        }
        return false;
    }

    public static function getUserId() {
        if (self::isAuth()) {
            return $_SESSION['user_id'];
        }
        return -1;
    }
}