<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.04.2017
 * Time: 23:58
 */
class FORUM {
    public static function getMainTopic() {
        $connect = DB_CONNECT::getInstance();
        $results = array();

        $sql = "SELECT * FROM main_topic";

        if (!$result = $connect->query($sql)) {
            $results['error'] = "Ошибка получения главных тем форума";
            LOG::sendLog("Ошибка получения главных тем форума");
        } else {
            if ($result->num_rows == 0) {
                $results['error'] = "Тем нет ..";
            } else {
                while ($row = $result->fetch_assoc()) {
                    $results['data'][] = $row;
                }
            }
        }
        return $results;
    }

    public static function getUserTopic($id) {
        $connect = DB_CONNECT::getInstance();
        $results = array();

        $sql = "SELECT * FROM user_topic WHERE topic_category_id = $id";

        if (!$result = $connect->query($sql)) {
            $results['error'] = "Ошибка получения пользователских тем форума";
            LOG::sendLog("Ошибка получения пользователских тем форума");
        } else {
            if ($result->num_rows == 0) {
                $results['error'] = "";
            } else {
                while ($row = $result->fetch_assoc()) {
                    $results['data'][] = $row;
                }
            }
        }
        return $results;
    }

    public static function addTopic($forum_id, $name, $description) {
        $connect = DB_CONNECT::getInstance();
        $results = array();

        $user_id = AUTH::getUserId();
        $forum_id = intval($forum_id);
        $name = $connect->escape(htmlspecialchars($name));
        $description = $connect->escape(htmlspecialchars($description));

        $data = "'$forum_id', '$user_id', '$name', '$description', 'NOW()'";
        $sql = "INSERT INTO user_topic (topic_category_id, user_id, topic_name, topic_description, topic_date_add) VALUES ($data)";

        if (!$result = $connect->query($sql)) {
            $results['error'] = "Ошибка при добавлении темы.";
            LOG::sendLog("Ошибка добавления новой темы", $connect->err());
        }
    }

    public static function getNews() {
        $connect = DB_CONNECT::getInstance();
        $results = array();

        $sql = "SELECT * FROM news";

        if (!$result = $connect->query($sql)) {
            $results['error'] = "Ошибка получения главных тем форума";
            LOG::sendLog("Ошибка получения главных тем форума");
        } else {
            if ($result->num_rows == 0) {
                $results['error'] = "Новостей нет";
            } else {
                while ($row = $result->fetch_assoc()) {
                    $results['data'][] = $row;
                }
            }
        }
        return $results;
    }
}