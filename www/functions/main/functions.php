<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.04.2017
 * Time: 22:38
 * @param $arr
 * @param $keys
 * @return bool
 */

function is($arr, $keys) {
    if (is_array($arr) && is_array($keys)) {
        foreach ($keys as $key) {
            if (!isset($arr[$key])) return false;
        }
        return true;
    }
    return false;
}

function isErr($r) {
    if (is_array($r) && isset($r['error'])) return true;
    else return false;
}

function len($str) {
    return mb_strlen($str, "utf-8");
}

function isLen($str, $min, $max) {
    if (len($str) < $min || len($str) > $max) return false;
    return true;
}