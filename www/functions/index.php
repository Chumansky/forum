<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.04.2017
 * Time: 20:54
 */

require_once $_SERVER['DOCUMENT_ROOT'] . "/functions/main/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/functions/main/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/functions/main/forum.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/functions/main/functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/functions/main/log.php';
